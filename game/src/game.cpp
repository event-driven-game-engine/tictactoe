#include "engine.h"
#include "required-service.h"
#include "ui-service.h"
#include <math.h>

RequiredService uisvc {"ui-service.dll"};

struct SpaceBarSubscriber : InputSubscriber
{
    SpaceBarSubscriber () : InputSubscriber(InputDef::KeyCode(SDLK_SPACE)) {}
    int presses = 0;
    void Execute () override
    {
        presses++;
        printf("space bar pressed %d time%s so far\n", presses, presses > 1 ? "s" : "");
    }
}
SpaceBarSubscriberInstance;

struct VoluntaryThrower : InputSubscriber
{
    VoluntaryThrower () : InputSubscriber(InputDef::KeyCode(SDLK_x)) {}
    void Execute () override
    {
        printf("throwing exception voluntarily\n");
        throw 123;
    }
}
VoluntaryThrowerInstance;

struct SampleStartSubscriber : Subscriber
{
    SampleStartSubscriber () : Subscriber(&EngineInstance->GameStart) {}
    void Execute () override
    {
        printf("game starting\n");
    }
}
SampleStartSubscriberInstance;

struct SampleFrameSubscriber : Subscriber
{
    SampleFrameSubscriber () : Subscriber(&EngineInstance->GameFrame) {}
    void Execute () override
    {
        //printf("game frame\n");
    }
}
SampleFrameSubscriberInstance;

struct SampleClosingSubscriber : Subscriber
{
    SampleClosingSubscriber () : Subscriber(&EngineInstance->GameClosing) {}
    void Execute () override
    {
        printf("game closing\n");
    }
}
SampleClosingSubscriberInstance;

struct Timing 
{
    Uint64 prev_time = SDL_GetPerformanceCounter();
    double delta = 0,
           elapsed = 0,
           interval;
    Timing (double interval = 1) : interval(interval) {}
    void Update ()
    {
        Uint64 now_time = SDL_GetPerformanceCounter();
        delta = (now_time - prev_time) / (double)SDL_GetPerformanceFrequency();
        prev_time = now_time;
        
        elapsed = fmod(elapsed + delta, interval);
    }
};

UIBox CenterBoxInstance {float2(0.2,0.2), float2(0.6,0.6)};
struct MovingBox : UIBox
{
    struct frame_subscribe_t : Subscriber
    {
        UIBox* box;
        frame_subscribe_t (UIBox* box) : box(box), Subscriber(&EngineInstance->GameFrame) {}
        Timing timing;
        void Execute () override 
        {
            timing.Update();
            float radians = timing.elapsed * M_PI * 2;
            box->top_left.x = 0.45 + cosf(radians) * 0.3;
            box->top_left.y = 0.45 - sinf(radians) * 0.3;
        }
    }
    frame_subscribe;
    MovingBox () 
        : UIBox(float2(0.2,0.5),float2(0.1,0.1)) 
        , frame_subscribe(this) 
    {}
}
MovingBoxInstance;

struct FrameCounter : Subscriber
{
    FrameCounter () : Subscriber(&EngineInstance->GameFrame) {}
    Uint64 start_time = SDL_GetPerformanceCounter();
    int frames = 0;
    float measure_interval = 1;
    void Execute () override 
    {
        frames++;
        Uint64 now_time = SDL_GetPerformanceCounter();
        double elapsed = (now_time - start_time) / (double)SDL_GetPerformanceFrequency();
        if (elapsed >= measure_interval)
        {
            start_time = now_time;
            printf("FPS: %.1f\n", (float)(frames / elapsed));
            frames = 0;
        }
    }
}
FrameCounterInstance;